package helloapp

import (
	"bufio"
	"os"
)

func CountLines(filePath string) (int, error) {
	fileHandler, err := os.Open(filePath)
	if err != nil {
		return 0, err
	}

	defer fileHandler.Close()

	fileScanner := bufio.NewScanner(fileHandler)

	fileScanner.Split(bufio.ScanLines)

	var lineSlice []string

	for fileScanner.Scan() {
		lineSlice = append(lineSlice, fileScanner.Text())
	}

	return len(lineSlice), nil
}

func CountWords(filePath string) (int, error) {
	fileHandler, err := os.Open(filePath)
	if err != nil {
		return 0, err
	}

	defer fileHandler.Close()

	fileScanner := bufio.NewScanner(fileHandler)

	fileScanner.Split(bufio.ScanWords)

	var wordSlice []string

	for fileScanner.Scan() {
		wordSlice = append(wordSlice, fileScanner.Text())
	}

	return len(wordSlice), nil
}
